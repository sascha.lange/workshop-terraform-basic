terraform {
  required_version = ">= 0.12, < 0.13"
}

provider "aws" {
  region = var.region

  # Allow any 2.x version of the AWS provider
  version = "~> 2.0"
}

module "webserver_cluster" {
  source = "../../../modules/services/webserver-cluster"
  cluster_name           = var.cluster_name

  instance_type = "t2.micro"
  min_size      = 2
  max_size      = 2
}
