#!/usr/bin/env bash
yum update -y
yum install git httpd -y
git clone https://github.com/Hextris/hextris.git /var/www/html/
systemctl restart httpd
