terraform {
  required_version = ">= 0.12, < 0.13"

  # backend "s3" {
  #   bucket = "sascha-terraform-state" # Must match created bucketname
  #   key    = "terraform"
  #   dynamodb_table = "sascha-terraform-example-lock"
  #   region = "eu-central-1"
  # }
}

provider "aws" {
  region = var.region
  version = "~> 2.0"
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = "${var.prefix}-terraform-state"

  force_destroy = true

  versioning {
    enabled = true
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_dynamodb_table" "terraform_state_lock" {
  name           = "${var.prefix}-terraform-example-lock"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
}

resource "aws_instance" "example" {
  ami                     = "ami-0cc293023f983ed53"
  instance_type           = "t2.micro"
  vpc_security_group_ids  = [ aws_security_group.webserver.id ]

  user_data = <<EOF
              #!/usr/bin/env bash
              yum update -y
              yum install git httpd -y
              git clone https://github.com/Hextris/hextris.git /var/www/html/
              systemctl restart httpd
              EOF

  tags = {
    Name = "${var.prefix}-terraform-example"
  }
}
