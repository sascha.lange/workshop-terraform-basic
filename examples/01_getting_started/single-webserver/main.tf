terraform {
  required_version = ">= 0.12, < 0.13"
}

provider "aws" {
  region = "eu-central-1"

  # Allow any 2.x version of the AWS provider
  version = "~> 2.0"
}

resource "aws_instance" "example" {
  ami                     = "ami-0cc293023f983ed53"
  instance_type           = "t2.micro"
  vpc_security_group_ids  = [ ]

  user_data = <<EOF
              #!/usr/bin/env bash
              yum update -y

              systemctl restart httpd
              EOF

  tags = {
    Name = "terraform-example"
  }
}
