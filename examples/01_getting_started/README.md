# Additional Information

## Copy and Paste Strings

| Description                | Values                                                   |
|----------------------------|----------------------------------------------------------|
| AMI ID                     | "ami-0cc293023f983ed53"                                  |
| Terraform Version          | ">= 0.12, < 0.13"                                        |
| AWS Region                 | "eu-central-1"                                           |
| ARN of AmazonEC2RoleforSSM | arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM |
| GIT resource               | https://github.com/Hextris/hextris.git                   |

## Official documentation

[Terraform settings](https://www.terraform.io/docs/configuration/terraform.html)

[Provider settings](https://www.terraform.io/docs/configuration/providers.html)

[Resource aws_instance](https://www.terraform.io/docs/providers/aws/r/instance.html)

[Resource aws_security_group](https://www.terraform.io/docs/providers/aws/r/security_group.html)
