variable "region" {
  type = string
}

variable "prefix" {
    description = "Prefix for resources were uniq names are required"
    type        = string
}
