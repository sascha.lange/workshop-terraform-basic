output "aws_user" {
  value = aws_iam_user.cloud9.name
}

output "aws_access_key_id" {
  value = aws_iam_access_key.cloud9.id
}

output "aws_secret_access_key" {
  value = aws_iam_access_key.cloud9.secret
}
