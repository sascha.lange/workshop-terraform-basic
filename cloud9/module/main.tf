terraform {
  required_version = ">= 0.12, < 0.13"
}

provider "aws" {
  region = "eu-central-1"

  # Allow any 2.x version of the AWS provider
  version = "~> 2.0"
}

data "aws_caller_identity" "current" {}

resource "aws_iam_user" "cloud9" {
  name  = var.user
}

resource "aws_iam_access_key" "cloud9" {
  user = aws_iam_user.cloud9.name
}

resource "aws_iam_user_policy_attachment" "cloud9-attach-policy" {
  user  = aws_iam_user.cloud9.name
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

resource "aws_cloud9_environment_ec2" "cloud9" {
  instance_type               = "t2.micro"
  automatic_stop_time_minutes = 420
  name                        = "${aws_iam_user.cloud9.name}-cloud9"
  owner_arn                   = aws_iam_user.cloud9.arn
}

