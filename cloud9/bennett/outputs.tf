output "aws_user" {
  value = module.cloud9_env.aws_user
}

output "aws_access_key_id" {
  value = module.cloud9_env.aws_access_key_id
}

output "aws_secret_access_key" {
  value = module.cloud9_env.aws_secret_access_key
}
